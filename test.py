from cstruct import Cstruct

test_frame = Cstruct({
         'size': {'type': 'uint8_t', 'value': 0},
         'type': {'type': 'uint8_t', 'value': 0x10},
         'result': {'type': 'uint32_t', 'value': 0xf1},
         'flags': {'type': 'uint8_t:flags', 
                'value': [
                            {'name': 'alarm', 'value': 0, 'position': 0, 'size': 1},
                            {'name': 'zaniki', 'value': 2, 'position': 1, 'size': 2}
                         ]
            }
        }, "Little")

if __name__ == '__main__':
    print(test_frame.get_value('result'))
    print(test_frame)
    stream = test_frame.get_stream()
    print(stream)
    test_frame.set_value('size', 14)
    test_frame.set_values([['size', 3], ['type', 9], ['result', 0x34]])
    print(test_frame.get_value('size'))
    print(test_frame)
    test_frame.parse([0x02, 0x10, 0x00, 0x00, 0x00, 0xf1, 0x04])
    print(test_frame.get_value('size'))
    print(test_frame)
